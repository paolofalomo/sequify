jQuery.fn.sequify = function (methodOrOptions) {
        var s = $(this);
        var sqf = {
            options: {
                preload: true,
                fps: 30,
                loop: true,
                selector: '> div',
                width: 640,
                height: 360,
                cover: false,
                maxLoadTime: 30,
                maxLoadTries: 1
            },
            init: function (options = {}) {
                s.trigger('sequify.init');
                sqf.updateOptions(options);
                sqf.images.set();
                if (sqf.options.preload) {
                    sqf.images.preload(function () {
                        sqf.sequence.start();
                    });
                } else {
                    sqf.sequence.start()
                }
            },
            getOptions: function () {
                return sqf.options;
            },
            updateOptions: function (options) {
                for (var k in options) {
                    if (k === 'fps') {
                        if (options[k] <= 0) {
                            jQuery.error('Frame Per Seconds cannot be lower or equal to 0');
                        }
                    } else {
                        sqf.options[k] = options[k];
                    }
                }
            },
            images: {
                data: [],
                loaded: false,
                loadTime: null,
                set: function () {
                    s.find(sqf.options.selector).each(function (i, image) {
                        sqf.images.data.push($(image).data('src'));
                    });
                },
                preload: function (preloadComplete = function(){}){
                    var microtime = (Date.now() % 1000) / 1000;
                    var images = sqf.images.data;
                    var loaded = 0;
                    var imageLoaders = [];
                    for (var i in images) {
                        imageLoaders[i] = {src: images[i]};
                        imageLoaders[i].dom = $('<img>');
                        imageLoaders[i].dom.attr("src", images[i]);
                        imageLoaders[i].dom.data("frame-id", i);
                        imageLoaders[i].dom.on('load', function () {
                            s.find(sqf.options.selector).eq($(this).data('frame-id')).addClass('sequify-loaded').css('background-image', 'url(' + $(this).attr('src') + ')');
                            loaded = loaded + 1;
                        });
                    }
                    var calcPercent = setInterval(function () {
                        sqf.images.loadTime = ((Date.now() % 1000) / 1000) - microtime;
                        if (images.length === loaded) {
                            sqf.images.loaded = true;
                            clearInterval(calcPercent);
                            s.trigger('sequify.preload.complete');
                            sqf.sequence.framesQuantity = images.length;
                            preloadComplete();
                        } else {
                            s.trigger('sequify.preload.loading');
                        }
                        if (sqf.images.loadTime > sqf.options.maxLoadTime / 2) {
                            jQuery.warn('preloading is taking more than half of maxLoadTime to load images');
                        }
                        if (sqf.images.loadTime >= sqf.options.maxLoadTime) {
                            clearInterval(calcPercent);
                            if (sqf.options.maxLoadTries > 0) {
                                jQuery.warn('preload taken more than maxLoadTime. Retrying in 3 seconds');
                                sqf.options.maxLoadTries = sqf.options.maxLoadTime - 1;
                                setTimeout(function () {
                                    sqf.init();
                                }, 3000);
                            } else {
                                jQuery.error('preload taken more than maxLoadTime. No more tries');
                            }
                        }
                    }, 10);
                },
            },
            sequence: {
                currentFrame: 0,
                framesQuantity: 0,
                intervalObject: null,
                showFrame: function (frameID) {
                    s.find(sqf.options.selector).eq(frameID).css('opacity', 1);
                },
                hideFrame: function (frameID) {
                    s.find(sqf.options.selector).eq(frameID).css('opacity', 0);
                },
                start: function () {
                    s.trigger('sequify.sequence.start');
                    sqf.sequence.intervalObject = setInterval(function () {
                        if (sqf.sequence.currentFrame < sqf.sequence.framesQuantity) {
                            if (s.find(sqf.options.selector).eq(sqf.sequence.currentFrame - 1).length) {
                                sqf.sequence.hideFrame(sqf.sequence.currentFrame - 1);
                            }
                            sqf.sequence.showFrame(sqf.sequence.currentFrame);
                            sqf.sequence.currentFrame++;
                        } else {
                            sqf.sequence.currentFrame = 0;
                        }
                    }, 1000 / sqf.options.fps);
                },
                stop: function () {
                    s.trigger('sequify.sequence.stop');
                    clearInterval(sqf.sequence.intervalObject);
                }
            }
        };

        /**
         * Event listeners
         */
        s.on('sequify.init', function () {
        });
        if (sqf[methodOrOptions]) {
            return sqf[methodOrOptions].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof methodOrOptions === 'object' || !methodOrOptions) {
            return sqf.init.apply(this, arguments);
        } else {
            jQuery.error('Method ' + methodOrOptions + ' does not exist on jQuery.sequify');
        }
    };